import sys
import os

sys.path.insert(0, os.path.join(os.path.abspath(os.path.dirname(__file__)), 'src'))

from app import create_db
from app import app as application


if __name__ == "__main__":
    create_db()
    application.run(threaded=True)
