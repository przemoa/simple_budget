from app import db, Category


def main():
    db.create_all()

    categories = [
        ('Inne', 'niezdefiniowane wydatki', 40, 40, True),
        ('Zakupy Codzienne', 'jedzenie, chemia domowa', 350, 350, True),
        ('Mieszkanie', 'rachunki, podatki nieruchomości', 350, 350, True),
        ('Platformy internetowe', 'spotify, netflix, hbo', 45, 45, True),
        ('Paliwo', '', 60, 60, True),
        ('Samochód', 'remonty, ubezpieczenie', 100, 100, True),
        ('Transport', 'MPK, PKP, rower', 120, 60, False),
        ('Zdrowie', 'lekarz, dentysta', 60, 80, False),
        ('Wydatki pracowe', 'zakupy, konferencje, publikacje [netto]', 80, 40, False),
        ('Kosmetyki', '', 20, 40, False),
        ('Uroda', 'Fryzjer, kosmetyczka', 20, 80, False),
        ('Wydatki zielonogórskie', 'internet, zakupy, drobne sprawy', 60, 80, False),
        ('Wakacje wspólne', '', 400, 400, True),
        ('Własne', '', 600, 800, False),
        ('Telefon', 'abonament', 35, 45, False),
    ]

    for category in categories:
        obj = Category(
            name=category[0],
            description=category[1],
            limit_dagmara=category[3],
            limit_przemek=category[2],
            common_limit=category[4],
        )
        db.session.add(obj)

    db.session.commit()
    print(Category.query.all())


if __name__ == '__main__':
    main()
