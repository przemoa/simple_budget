# -*- coding: future_fstrings -*-


import enum
import os
import shutil

from flask import Flask, render_template, request, redirect, Response
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime


app = Flask(__name__)
DB_RELATIVE_PATH = '../db_data/simple_budget_v1.db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + DB_RELATIVE_PATH
db = SQLAlchemy(app)


EXPENDITURES_BETWEEN_BAKCUPS = 10


def bool2str(val):
    return str(val).lower()


class MessageForUserException(Exception):
    pass


class OwnerType(enum.Enum):
    dagmara = 1
    przemek = 2
    razem = 3   # razem==common; idk, no idea for fast quick language handling

    @staticmethod
    def from_string(txt: str):
        for e in OwnerType:
            if e.name.lower() == txt.lower():
                return e
        raise Exception(f"Invalid OwnerType '{txt}'!")

    def fits_budget_owner(self, item) -> bool:
        if self == OwnerType.razem:
            return True

        if isinstance(item, Category):
            return not item.common_limit
        if isinstance(item, Expenditure):
            if item.get_category().common_limit:
                return False
            else:
                return bool(item.get_owner() == self)
        return False

    @classmethod
    def get_all_names(cls):
        return [p.name for p in cls]


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    description = db.Column(db.String(200), nullable=False)
    limit_dagmara = db.Column(db.Integer, nullable=False)
    limit_przemek = db.Column(db.Integer, nullable=False)
    common_limit = db.Column(db.Boolean, nullable=False)

    def get_display_name(self):
        return f'{self.id}. {self.name}'

    def get_budget_str(self):
        if self.common_limit:
            return f'{self.limit_dagmara + self.limit_przemek}'
        else:
            return f'{self.limit_dagmara}&{self.limit_przemek}'


class Expenditure(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    description = db.Column(db.String(200), nullable=False)
    owner = db.Column(db.Integer, nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'), nullable=False)
    amount = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    paid_by_other = db.Column(db.Boolean, nullable=False)

    def get_owner_letter(self):
        return OwnerType(self.owner).name[0].upper()

    def get_owner(self):
        return OwnerType(self.owner)

    def get_category(self) -> Category:
        return Category.query.get(self.category_id)

    def gey_date_yyyy_mm_dd(self):
        created_at = self.created_at
        txt = created_at.strftime("%Y-%m-%d")
        return txt

    def is_owner_przemek_str(self):
        return bool2str(OwnerType(self.owner) == OwnerType.przemek)

    def is_owner_dagmara_str(self):
        return bool2str(OwnerType(self.owner) == OwnerType.dagmara)

    def paid_by_other_str(self):
        return bool2str(OwnerType(self.owner) == OwnerType.dagmara)


class CategoryBudgetSpan:
    def __init__(self, category: Category, spent=0, planned=0):
        self.spent = spent
        self.planned = planned
        self.category = category

    def get_savings_color_level(self):
        try:
            percentage_spent = self.spent / self.planned
        except ZeroDivisionError:
            return "table-light"

        if percentage_spent < 0.8:
            return "table-success"
        elif percentage_spent < 0.9:
            return "table-light"
        elif percentage_spent < 1.05:
            return "table-warning"
        elif percentage_spent < 1.2:
            return "table-secondary"
        else:
            return "table-danger"

    def get_budget_ratio(self):
        try:
            ratio = self.spent / self.planned
            return f'{ratio:.2f}'
        except ZeroDivisionError:
            return "1"

    def get_budget_str(self):
        spent = f'{round(self.spent)}' if self.spent < 9999 else f'{self.spent/1000:.1f}k'
        planned = f'{round(self.planned)}' if self.planned < 9999 else f'{self.planned/1000:.1f}k'
        return f'{spent} / {planned}'

    def add_expenditure(self, expenditure: Expenditure):
        self.spent += expenditure.amount

    def calculate_limit_for_owner(self, whose_budget, time_distance_days):
        if self.category.common_limit or whose_budget == OwnerType.razem:
            monthly_limit = self.category.limit_przemek + self.category.limit_dagmara
        elif whose_budget == OwnerType.dagmara:
            monthly_limit = self.category.limit_dagmara
        elif whose_budget == OwnerType.przemek:
            monthly_limit = self.category.limit_przemek

        daily_limit = monthly_limit * 12 / 365
        self.planned = daily_limit * time_distance_days


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/latest_expenditures.html')
def get_page_latest_expenditures():
    if request.args:
        date_from = datetime.strptime(request.args['date_from'], "%Y-%m-%d")
        date_to = datetime.strptime(request.args['date_to'], "%Y-%m-%d")
    else:
        date_from = datetime.now().replace(day=1)
        date_to = datetime.now()

    date_from_str = date_from.strftime("%Y-%m-%d")
    date_to_str = date_to.strftime("%Y-%m-%d")

    recent_expenditures = db.session.query(Expenditure) \
                              .filter(Expenditure.created_at >= date_from) \
                              .filter(Expenditure.created_at <= date_to) \
                              .order_by(Expenditure.created_at.desc()) \
                              .limit(500)[::1]

    return render_template('latest_expenditures.html', 
                           recent_expenditures=recent_expenditures,
                           date_to_str=date_to_str,
                           date_from_str=date_from_str)


@app.route('/categories_overview.html')
def get_page_categories_overview():
    categories = Category.query.all()
    return render_template('categories_overview.html', categories=categories)


@app.route('/budget_overview.html')
def get_page_budget_overview():
    if request.args:
        date_from = datetime.strptime(request.args['date_from'], "%Y-%m-%d")
        date_to = datetime.strptime(request.args['date_to'], "%Y-%m-%d")
        whose_budget_str = request.args['whose_budget']
        whose_budget = OwnerType.from_string(whose_budget_str)
    else:
        date_from = datetime.now().replace(day=1)
        date_to = datetime.now()
        whose_budget = OwnerType.razem

    time_distance = date_to - date_from
    time_distance_days = time_distance.days

    date_from_str = date_from.strftime("%Y-%m-%d")
    date_to_str = date_to.strftime("%Y-%m-%d")

    expenditures = db.session.query(Expenditure)\
        .filter(Expenditure.created_at >= date_from)\
        .filter(Expenditure.created_at <= date_to)\
        .all()

    categories = Category.query.all()
    category_budgets = [CategoryBudgetSpan(category=c) for c in categories
                        if whose_budget.fits_budget_owner(c)]
    category_budgets_dict = {cb.category.id: cb for cb in category_budgets}

    for expenditure in expenditures:
        if not whose_budget.fits_budget_owner(expenditure):
            continue
        category_budgets_dict[expenditure.category_id].add_expenditure(expenditure)

    for category_budget in category_budgets:
        category_budget.calculate_limit_for_owner(whose_budget, time_distance_days)

    class SummaryCategory:
        id = '-'
        name = 'Suma'

    summary_category_budget = CategoryBudgetSpan(category=SummaryCategory)
    summary_category_budget.planned = sum([cb.planned for cb in category_budgets])
    summary_category_budget.spent = sum([cb.spent for cb in category_budgets])

    daily_savings = (summary_category_budget.planned - summary_category_budget.spent) / time_distance_days
    summary_info = f'Zakres czasu: {time_distance_days * 12 / 365:.1f} miesięcy ({time_distance_days} dni)<br>' \
                   f'Oszczędności dziennie: {daily_savings:.2f} PLN'

    return render_template('budget_overview.html',
                           date_from_str=date_from_str,
                           date_to_str=date_to_str,
                           whose_budgets=[t.title() for t in OwnerType.get_all_names()],
                           whose_budget=whose_budget.name.title(),
                           category_budgets=category_budgets,
                           summary_category_budget=summary_category_budget,
                           summary_info=summary_info)


def parse_expenditure(req):
    category_id = int(req.form['category'].split('.')[0])
    if not req.form['amount_pln']:
        raise MessageForUserException("Brak kwoty wydatku!")

    amount = int(req.form['amount_pln'])
    description = req.form['description']
    name = req.form['name']
    created_at = datetime.strptime(req.form['created_at'], "%Y-%m-%d")
    owner = OwnerType.from_string(req.form['owner'])
    if 'paid_by_other' in req.form and 1:
        paid_by_other = req.form['paid_by_other'] == 'on'
    else:
        paid_by_other = False

    new_expenditure = Expenditure(name=name,
                                  description=description,
                                  owner=owner.value,
                                  category_id=category_id,
                                  amount=amount,
                                  created_at=created_at,
                                  paid_by_other=paid_by_other)
    return new_expenditure


@app.route('/add_expenditure.html', methods=['GET', 'POST'])
def get_page_add_expenditure():
    global EXPENDITURES_BETWEEN_BAKCUPS
    global DB_RELATIVE_PATH

    last_added_text = None

    if request.method == 'POST':
        try:
            new_expenditure = parse_expenditure(request)
            db.session.add(new_expenditure)
            db.session.commit()
            last_added_text = f'Dodano wydatek {new_expenditure.amount} PLN!'
            if new_expenditure.id % EXPENDITURES_BETWEEN_BAKCUPS == 0:
                backup_relative_path = DB_RELATIVE_PATH + f'_{new_expenditure.id}'
                shutil.copyfile(DB_RELATIVE_PATH, backup_relative_path)
        except Exception as e:
            error_text = f'ERROR! Nieudane dodanie wydatku! <br><br>' \
                         f'"{e}"'
            return render_template('error_page.html', error_text=error_text)

    categories = Category.query.all()
    return render_template('add_expenditure.html', categories=categories, last_added_text=last_added_text)


def update_item(current_item, new_item):
    table = current_item.__table__
    non_pk_columns = [k for k in table.columns.keys() if k not in table.primary_key]
    for c in non_pk_columns:
        setattr(current_item, c, getattr(new_item, c))


@app.route('/edit_expenditure.html', methods=['GET', 'POST'])
def get_page_edit_expenditure():
    expenditure_id = int(request.args['id'])
    expenditure = Expenditure.query.get(expenditure_id)

    if request.method == 'POST':
        try:
            modified_expenditure = parse_expenditure(request)
            update_item(expenditure, modified_expenditure)
            db.session.commit()
        except Exception as e:
            error_text = f'ERROR! Nieudana edycja wydatku! <br><br>' \
                         f'"{e}"'
            return render_template('error_page.html', error_text=error_text)

        return redirect('/latest_expenditures.html')
    else:
        categories = Category.query.all()
        return render_template('edit_expenditure.html', categories=categories, expenditure=expenditure)


@app.route('/expenditure/delete/<int:expenditure_id>')
def delete_expenditure(expenditure_id):
    try:
        expenditure = Expenditure.query.get(expenditure_id)
        db.session.delete(expenditure)
        db.session.commit()
    except Exception as e:
        error_text = f'ERROR! Nieudana próba usunięcia wydatku! <br><br>' \
                     f'"{e}"'
        return render_template('error_page.html', error_text=error_text)

    return redirect('/latest_expenditures.html')


@app.route("/expenditures_csv")
def get_expnditures_csv():
    csv = 'id,name,description,owner,category_id,amount,created_at,paid_by_other\n'
    expenditures = Expenditure.query.all()
    for e in expenditures:
        csv += f'{e.id},{e.name},{e.description},{e.owner},{e.category_id},' \
               f'{e.amount},{e.created_at},{e.paid_by_other}\n'

    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=simple_budget_expenditures.csv"})

def create_db():
    if os.path.isfile(os.path.join(os.path.dirname(__file__), DB_RELATIVE_PATH)):
        print('Database already exists!')
    else:
        import prepare_db
        prepare_db.main()


def main():
    create_db()
    app.run(debug=True, host='0.0.0.0')


if __name__ == '__main__':
    main()
