# simple_budget

# Development:
```
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt

python app.py
```

# Deployment
- Installing mod_wsgi (`sudo apt install libapache2-mod-wsgi-py3`)
- `cd /var/www/vhosts/; git clone xxxx`
- python3 `-m venv venv; . venv/bin/activate; pip install -r requirements.txt`
- test in place: `python app.py`
- copy conf file to '/etc/apache2/sites-available' (`cp simple_budget.conf /etc/apache2/sites-available/`)
- add 'Listen 34568' to `/etc/apache2/ports.conf`
- `sudo a2ensite simple_budget  && sudo service apache2 reload && sudo apachectl restart`
- `chmod -R 777 /var/www/vhosts/simple_budget/db_data/`
- test web page

